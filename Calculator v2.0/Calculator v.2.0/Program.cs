﻿using CalcBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_v._2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager(new DigitCalc());
            manager.DoWork();
        }
    }
}
