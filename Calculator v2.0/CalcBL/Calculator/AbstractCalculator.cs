﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public abstract class AbstractCalculator
    {
        public event EventHandler<MyEventArgs> ShowLog;
        private delegate string OperationDelegate(string arg1, string arg2);
        private Dictionary<Operation, OperationDelegate> operations;

        public AbstractCalculator()
        {
            this.operations = new Dictionary<Operation, OperationDelegate>
               {
            { Operation.Addition, this.Addition },
            { Operation.Subtract, this.Subtract },
            {Operation.Multiply, this.Multiply },
            { Operation.Devide, this.Devide },
               };
        }
        public string PerformOperation(Parcer parcer)
        {
            if (!operations.ContainsKey(parcer.LastOperation))
                throw new ArgumentException(string.Format("Operation {0} is invalid", parcer.LastOperation), "op");
            string res = operations[parcer.LastOperation](parcer.PreviousValue, parcer.Current);
            OnShowLog(new MyEventArgs(), parcer.PreviousValue, parcer.Current,parcer.LastOperation.ToString(), res);
            return res;
        }
        public string Subtract(string arg1, string arg2)
        {
            var arg1Num = ConvertToDesiredType(arg1);
            var arg2Num = ConvertToDesiredType(arg2);
            var res = arg1Num - arg2Num;
            return Convert.ToString(res);
        }

        protected abstract dynamic ConvertToDesiredType(string s);

        public  string Addition(string arg1, string arg2)
        {
            var arg1Num = ConvertToDesiredType(arg1);
            var arg2Num = ConvertToDesiredType(arg2);
            var res = arg1Num + arg2Num;
            return Convert.ToString(res);
        }
        public  string Devide(string arg1, string arg2)
        {
            var arg1Num = ConvertToDesiredType(arg1);
            var arg2Num = ConvertToDesiredType(arg2);
            var res = arg1Num / arg2Num;
            return Convert.ToString(res);
        }
        public  string Multiply(string arg1, string arg2)
        {
            var arg1Num = ConvertToDesiredType(arg1);
            var arg2Num = ConvertToDesiredType(arg2);
            var res = arg1Num * arg2Num;
            return Convert.ToString(res);
        }

        protected virtual void OnShowLog(MyEventArgs e, string arg1, string arg2, string operation, string res)
        {
            e.Message = ActivityLog.ShowLog(arg1, arg2, operation, res);
            ShowLog?.Invoke(this, e);
        }
    }
}
