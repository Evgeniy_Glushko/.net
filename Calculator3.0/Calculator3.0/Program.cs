﻿using System;
using System.Globalization;
using System.Threading;
using CalcBL;

namespace Calculator3._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Manager manager = new Manager(new DigitParcer());
            manager.DoWork();
            Console.ReadKey();
        }
    }
}
