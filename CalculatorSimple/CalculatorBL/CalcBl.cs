﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBL
{

    public class CalcBl
    {
        Calculation calculation = new Calculation();
        public string Calculate (double arg1, double arg2, string oper)
        {
            Operation enumOper = oper.ConvertToButtonType();
            switch (enumOper)
            {
                case Operation.Addition: return Convert.ToString(calculation.Addition(arg1, arg2));
                case Operation.Devide: return Convert.ToString(calculation.Devide(arg1, arg2));
                case Operation.Multiply: return Convert.ToString(calculation.Multiply(arg1, arg2));
                case Operation.Subtract: return Convert.ToString(calculation.Subtract(arg1, arg2));
                default: throw new ExceptionsCalc("Operation not found");
            }
        }
    }
}
