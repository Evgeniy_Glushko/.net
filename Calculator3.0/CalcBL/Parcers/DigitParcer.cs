﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class DigitParcer : AbstractParcer
    {
        protected override bool IsDigit(char button)
        {
            if (char.IsDigit(button) || button == '.')
                return true;
            else return false;
        }

        protected override void BtnDigit(char btn)
        {
            if (Clear)
            {
                if (btn == '.')
                {
                    Current = "0" + btn;
                }
                else
                    Current = Convert.ToString(btn);
            }
            else
            {
                if (btn == '.' && Current.IndexOf(",", StringComparison.Ordinal) >= 0)
                    return;
                Current += btn;
            }
            Clear = false;
        }

        protected override bool BtnIsOperation(Operation btn)
        {
            foreach (DigitOperation val in Enum.GetValues(typeof (DigitOperation)))
            {
                if (val == (DigitOperation) btn)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
