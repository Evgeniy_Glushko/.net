﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    class ActivityLog
    {
        public static string ShowLog(string arg1,string arg2, string operation, string res)
        {
            string log;
            log = arg1 +" "+ operation +" "+ arg2 + " = \n" + res + "\n\n";
            return log;
        }
    }
}
