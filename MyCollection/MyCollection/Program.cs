﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using HandBookBL;
using System.Xml.Serialization;

namespace MyCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new Manager();
            manager.LoadData();
            manager.DoWork();
            manager.SaveData();
        }
    }
}