﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCollection
{
        public delegate void SaveToFile<in T>(string fileName, T data);
        public delegate T LoadFromFile<out T>(string fileName);
}
