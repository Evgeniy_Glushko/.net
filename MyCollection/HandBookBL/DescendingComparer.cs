﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandBookBL
{
    public class DescendingComparer : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            int result = String.CompareOrdinal(y.Author.ToUpper(), x.Author.ToUpper());
            return result;
        }
    }
}
