﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HandBookBL;

namespace MyCollection
{
    internal class Manager
    {
        XmlFilesHandling<Collection> _filesHandling = new XmlFilesHandling<Collection>();
        Collection _handBook = new Collection();
        private readonly SaveToFile<Collection> save;
        private readonly LoadFromFile<IEnumerable<Book>> load; 
        public Manager()
        {
            save += _filesHandling.SaveToFile;
            load += _filesHandling.LoadFromFile;
        }

        public void ShowBooks(object sender, EventArgs args)
        {
            //var collection = sender as Collection;
            //if (collection != null)
            //    foreach (var item in collection)
            //    {
            //        Console.WriteLine(item);
            //    }
            Console.WriteLine(List._9);
            Console.WriteLine(List._15);
            Console.WriteLine(List._9);
        }

        internal void LoadData()
        {
            var data = load("books.xml");
            if (data != null)
                _handBook = data as Collection;
            _handBook.MyEvent += ShowBooks;
        }

        public void DoWork()
        {
            bool state = true;
            try
            {
                while (state)
                {
                    int choice = Actions();
                    string title;
                    string author;
                    int year;
                    switch (choice)
                    {
                        case 1:
                            Console.Write(List._10);
                            title = Console.ReadLine();
                            Console.Write(List._11);
                            author = Console.ReadLine();
                            Console.Write(List._12);
                            year = Convert.ToInt32(Console.ReadLine());
                            _handBook.Add(new Book(title, author, year));
                            break;
                        case 2:
                            Console.Write(List._11);
                            author = Console.ReadLine();
                            Console.WriteLine(_handBook.Search(author));
                            break;
                        case 3:
                            Console.Write(List._10);
                            title = Console.ReadLine();
                            _handBook.RemoveByTitle(title);
                            break;
                        case 4:
                            Console.Write(List._10);
                            title = Console.ReadLine();
                            Console.Write(List._13);
                            year = Convert.ToInt32(Console.ReadLine());
                            _handBook.ChangeYear(title, year);
                            break;
                        case 5:
                            Console.WriteLine(List._14);
                            int i = Convert.ToInt32(Console.ReadLine());
                            if (i == 1)
                                _handBook.Sort(new DescendingComparer());
                            else
                                _handBook.Sort();
                            foreach (var item in _handBook)
                            {
                                Console.WriteLine(item);
                                //
                                Console.WriteLine(List._9);
                            }
                            break;
                        case 6:
                            _handBook.Clear();
                            break;
                        case 7:
                            state = false;
                            break;
                        default:
                            state = false;
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }
        private int Actions()
        {
            StringBuilder selAction = new StringBuilder();
            selAction.AppendLine(List._1);
            selAction.AppendLine(List._2);
            selAction.AppendLine(List._3);
            selAction.AppendLine(List._4);
            selAction.AppendLine(List._5);
            selAction.AppendLine(List._6);
            selAction.AppendLine(List._7);

            Console.WriteLine(selAction.ToString());
            Console.Write(List._8);
            return Convert.ToInt16(Console.ReadLine());
        }
        internal void SaveData()
        {
            save("books.xml", _handBook);
        }
    }
}
