﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    [Serializable]
    public delegate string MyEventHandler<in TEventArgs>(object sender, TEventArgs e);
    public abstract class AbstractInvoker
    {
        private Operation _lastOperation = Operation.None;
        private string _currentValue = "0";
        private string _previousValue;
        public event MyEventHandler<ParcerEventArgs> CalculateEvent;

        public string Current
        {
            get { return _currentValue; }
            protected set { _currentValue = value; }
        }
        
        private string PreviousValue => _previousValue;

        protected bool Clear { get; set; } = true;

        public void InvokeBtn(char button)
        {

            int btnInt = button;
            Operation btn = (Operation)btnInt;
            if (IsDigit(button))
                BtnDigit(button);
            else if (BtnIsOperation(btn))
                BtnOperation(btn);
        }

        protected abstract bool IsDigit(char button);

        protected abstract void BtnDigit(char button);

        private bool BtnIsOperation<T>(T btn)
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Any(val => Equals(val, (T)btn));
        }

        private void BtnOperation(Operation btn)
        {
            switch (btn)
            {
                case Operation.Delete:
                    if (_currentValue.Length > 1 && Clear == false)
                    {
                        _currentValue = _currentValue.Remove(_currentValue.Length - 1, 1);
                        Clear = false;
                        return;
                    }
                    _currentValue = "0";
                    Clear = true;
                    break;
                default:
                    Calculate(btn);
                    Clear = true;
                    break;
            }
        }
        private void Calculate(Operation oper, bool exprOne = false)
        {
            if (exprOne)
                _lastOperation = oper;
            if (!Clear)
            {
                Calculate();
                _previousValue = _currentValue;
            }
            _lastOperation = oper;
        }

        private void Calculate()
        {
            if (_lastOperation == Operation.None || _lastOperation == Operation.Equal)
                return;
            _currentValue = CalculateValue(_lastOperation);
        }

        private string CalculateValue(Operation oper)
        {
            return OnCalculateEvent(new ParcerEventArgs(PreviousValue, Current, oper));
        }

        protected void ChekNewline(char btn)
        {
            if (Clear)
            {
                _currentValue = btn.Equals('.') ? "0" + btn : Convert.ToString(btn);
            }
        }

        protected virtual string OnCalculateEvent(ParcerEventArgs e)
        {
            return CalculateEvent?.Invoke(this, e);
        }
    }
}
