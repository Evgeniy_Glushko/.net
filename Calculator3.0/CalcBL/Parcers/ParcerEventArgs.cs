﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class ParcerEventArgs
    {
        private string _arg1;
        private string _arg2;
        private Operation _oper;
        public string Arg1 { get { return this._arg1; } }
        public string Arg2 { get { return this._arg2; } }
        public Operation Oper { get { return this._oper; } }

        public ParcerEventArgs(string arg1, string arg2, Operation oper)
        {
            _arg1 = arg1;
            _arg2 = arg2;
            _oper = oper;
        }


    }
}
