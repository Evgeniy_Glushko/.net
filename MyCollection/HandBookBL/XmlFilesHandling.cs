﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HandBookBL
{
    public class XmlFilesHandling<T> where T: class 
    {
        readonly XmlSerializer _serializer = new XmlSerializer(typeof(T));
        public void SaveToFile(string fileName, IEnumerable<Book> data)
        {
            using (var fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                _serializer.Serialize(fs, data);
            }
        }
        public T LoadFromFile(string fileName)
        {
            if (!File.Exists(fileName)) return null;
            using (var fs = File.OpenRead(fileName))
            {
                return _serializer.Deserialize(fs) as T;
            }
        }
    }
}
