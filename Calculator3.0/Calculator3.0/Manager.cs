﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL;

namespace Calculator3._0
{
    class Manager
    {
        private AbstractParcer parcer;

        public Manager(AbstractParcer parcer)
        {
            this.parcer = parcer;
            parcer.CalculateEvent += Calculate;
        }

        private string Calculate(object sender, ParcerEventArgs e)
        {
            ICalculatorBase calc = new DigitCalc();
            return calc.PerformOperation(e.Arg1, e.Arg2, e.Oper);
        }

        public void DoWork()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine(parcer.Current);
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    parcer.InvokeBtn(key.KeyChar);
                    Console.Clear();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
