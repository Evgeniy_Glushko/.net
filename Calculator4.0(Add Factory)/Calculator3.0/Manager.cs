﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL;

namespace Calculator3._0
{
    class Manager
    {
        private readonly AbstractInvoker _invoker;
        private ICalculation _calc;

        public Manager(AbstractFactory factory)
        {
            _invoker = factory.CreateInvoker();
            _calc = factory.CreateCalculation();
            _invoker.CalculateEvent += Calculate;
        }

        private string Calculate(object sender, ParcerEventArgs e)
        {
            return _calc.PerformOperation(e.Arg1, e.Arg2, e.Oper);
        }

        public void Run()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine(_invoker.Current);
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    _invoker.InvokeBtn(key.KeyChar);
                    Console.Clear();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
