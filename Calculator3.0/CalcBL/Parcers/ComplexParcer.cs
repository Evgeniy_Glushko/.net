﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CalcBL
{
    public class ComplexParcer:AbstractParcer
    {
        protected override bool IsDigit(char button)
        {
            if (char.IsDigit(button) || button.Equals(',') || button.Equals(' '))
                return true;
            else return false;
        }

        protected override void BtnDigit(char btn)
        {
            int amountDot = new Regex(@"\,").Matches(Current).Count;
            int amountSpace = new Regex(@" ").Matches(Current).Count;
           // int amountJ = new Regex(@"j").Matches(currentValue).Count;
            if (Clear)
            {
                if (btn.Equals(','))
                {
                    Current = "0" + btn;
                }
                else
                    Current = Convert.ToString(btn);
            }
            else
            {
                if ((btn.Equals(',') && amountDot > 2) || (btn.Equals(' ') && amountSpace > 1))
                    return;
                Current += btn;
            }
            Clear = false;
        }

        protected override bool BtnIsOperation(Operation btn)
        {
            foreach (ComplexOperation val in Enum.GetValues(typeof(ComplexOperation)))
            {
                if (val == (ComplexOperation)btn)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
