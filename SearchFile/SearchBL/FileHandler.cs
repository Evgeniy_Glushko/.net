﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SearchBL
{
    public class FileHandler
    {
        readonly BinaryFormatter _formatter = new BinaryFormatter();
        public void SaveToFile(string fileName, IList<IList<FileInfo>> data)
        {
            using (var fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                _formatter.Serialize(fs, data);
            }
        }
        public IList<IList<FileInfo>> LoadFromFile(string fileName)
        {
            if (!File.Exists(fileName)) return null;
            using (var fs = File.OpenRead(fileName))
            {
                return _formatter.Deserialize(fs) as IList<IList<FileInfo>>;
            }
        }

        public void FileCompression(string fileName)
        {
            FileInfo fileToCompressing = new FileInfo(fileName);
            using (FileStream originalFileStream = fileToCompressing.OpenRead())
            {
                if ((File.GetAttributes(fileToCompressing.FullName) &
                   FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompressing.Extension != ".gz")
                {
                    using (FileStream compressedFileStream = File.Create(fileToCompressing.FullName + ".gz"))
                    {
                        using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                           CompressionMode.Compress))
                        {
                            originalFileStream.CopyTo(compressionStream);

                        }
                    }
                    FileInfo info = new FileInfo(fileToCompressing.Name + ".gz");
                    Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                    fileToCompressing.Name, fileToCompressing.Length.ToString(), info.Length.ToString());
                }

            }
        }
    }
}
