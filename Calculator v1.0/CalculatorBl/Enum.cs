﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBl
{
    public enum ButtonType
    {

        Zero = 48,
        One = 49,
        Two = 50,
        Three = 51,
        Four = 52,
        Five = 53,
        Six = 54,
        Seven = 55,
        Eight = 56,
        Nine = 57,
        Delimeter = 46,

        Addition = 43,
        Devide = 47,
        Multiply = 42,
        Subtract = 45,
        Delete = 8,
        Equal = 13,
        Insert = 45,
        Spacebar = 32

    }

    public enum Operation
    {
        Addition = 43,
        Devide = 47,
        Multiply = 42,
        Subtract = 45,
        Delete = 8,
        Equal = 13,
        None = 0,
        Insert = 45
    }
}
