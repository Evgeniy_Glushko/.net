﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CalcBL
{
    public class ComplexCalc: ICalculatorBase
    {
        private delegate string OperationDelegate(string arg1, string arg2);
        private Dictionary<Operation, ComplexCalc.OperationDelegate> operations;

        public ComplexCalc()
        {
            this.operations = new Dictionary<Operation, ComplexCalc.OperationDelegate>
               {
            { Operation.Addition, this.Addition },
            { Operation.Subtract, this.Subtract },
            {Operation.Multiply, this.Multiply },
            { Operation.Devide, this.Devide },
               };
        }
        public string PerformOperation(string arg1, string arg2, Operation oper)
        {
            if (!operations.ContainsKey(oper))
                throw new ArgumentException(string.Format("Operation {0} is invalid", oper), "op");
            string res = operations[oper](arg1, arg2);
            // OnShowLog(new MyEventArgs(), parcer.PreviousValue, parcer.Current, parcer.LastOperation.ToString(), res);
            return res;
        }
        public string Subtract(string arg1, string arg2)
        {
            Complex arg1Num = arg1.ConvertTo();
            Complex arg2Num = arg2.ConvertTo();
            Complex res = arg1Num - arg2Num;
            return Convert.ToString(res);
        }
        public string Addition(string arg1, string arg2)
        {
            Complex arg1Num = arg1.ConvertTo();
            Complex arg2Num = arg2.ConvertTo();
            Complex res = arg1Num + arg2Num;
            return Convert.ToString(res);
        }
        public string Devide(string arg1, string arg2)
        {
            Complex arg1Num = arg1.ConvertTo();
            Complex arg2Num = arg2.ConvertTo();
            Complex res = arg1Num / arg2Num;
            return Convert.ToString(res);
        }
        public string Multiply(string arg1, string arg2)
        {
            Complex arg1Num = arg1.ConvertTo();
            Complex arg2Num = arg2.ConvertTo();
            Complex res = arg1Num * arg2Num;
            return Convert.ToString(res);
        }
    }
}

