﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class Parcer
    {
        private Operation lastOperation = Operation.None;
        private string currentValue = "0";
        protected string previousValue;
        protected string memoryValue;
        protected bool clear = true;
        public string btnContent = "";
        //public AbstractCalculator calc;
        public string Current
        {
            get { return this.currentValue; }
        }

        public string PreviousValue
        {
            get { return this.previousValue; }
        }
        public Operation LastOperation
        {
            get { return this.lastOperation; }
        }
        public Parcer(AbstractCalculator calc)
        {
            this.calc = calc;
        }

        public void InvokeBtn(char button)
        {

            int btnInt = (int)button;
            Operation btn = (Operation)btnInt;
            btnContent = Convert.ToString(button);
            if (char.IsDigit(button) || button == ',' || button == ' ')
                BtnDigit(button);
            else if (BtnIsOperation(btn))
                BtnOperation(btn);
            else
                return;
        }
        private bool BtnIsOperation(Operation btn)
        {
            foreach (Operation val in Enum.GetValues(typeof(Operation)))
            {
                if (val == (Operation)btn)
                {
                    return true;
                }
            }
            return false;
        }
        public void BtnDigit(char btn)
        {

            if (clear)
            {
                if (btn == ',')
                {
                    currentValue = "0" + btn;
                }
                else
                    currentValue = Convert.ToString(btn);
            }
            else
            {
                currentValue += btn;
            }
            clear = false;
        }
        public void BtnOperation(Operation btn)
        {
            switch (btn)
            {
                case Operation.Delete:
                    if (currentValue.Length > 1 && clear == false)
                    {
                        currentValue = currentValue.Remove(currentValue.Length - 1, 1);
                        clear = false;
                        return;
                    }
                    else
                    {
                        currentValue = "0";
                        clear = true;
                    }
                    break;
                default:
                    Calculate(btn);
                    clear = true;
                    break;
            }
        }
        private void Calculate(Operation oper, bool exprOne = false)
        {
            if (exprOne)
                lastOperation = oper;
            if (!clear)
            {
                Calculate();
                previousValue = currentValue;
            }
            lastOperation = oper;
        }

        private void Calculate()
        {
            if (lastOperation == Operation.None || lastOperation == Operation.Equal)
                return;
            currentValue = CalculateValue(lastOperation);
        }

        private string CalculateValue(Operation oper)
        {
            return calc.PerformOperation(this);
        }
    }
}
