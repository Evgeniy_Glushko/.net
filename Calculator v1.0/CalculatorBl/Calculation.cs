﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBl
{
    interface ICalculationSimple
    {
        double Subtract(double arg1, double arg2);
        double Addition(double arg1, double arg2);
        double Multiply(double arg1, double arg2);
        double Devide(double arg1, double arg2);
    }
    interface ICalculationSimpleComplex
    {
        Complex Subtract(Complex arg1, Complex arg2);
        Complex Addition(Complex arg1, Complex arg2);
        Complex Multiply(Complex arg1, Complex arg2);
        Complex Devide(Complex arg1, Complex arg2);
    }

    public class Calculation : ICalculationSimple, ICalculationSimpleComplex
    {
        public double Subtract(double arg1, double arg2)
        {
            return arg1 - arg2;
        }
        public Complex Addition(Complex arg1, Complex arg2)
        {
            return arg1 + arg2;
        }

        public Complex Devide(Complex arg1, Complex arg2)
        {
            return arg1 / arg2;
        }

        public Complex Multiply(Complex arg1, Complex arg2)
        {
            return arg1 * arg2;
        }

        public Complex Subtract(Complex arg1, Complex arg2)
        {
            return arg1 - arg2;
        }
        public double Addition(double arg1, double arg2)
        {
            return arg1 + arg2;
        }
        public double Multiply(double arg1, double arg2)
        {
            return arg1 * arg2;
        }
        public double Devide(double arg1, double arg2)
        {
            return arg1 / arg2;
        }

    }

}
