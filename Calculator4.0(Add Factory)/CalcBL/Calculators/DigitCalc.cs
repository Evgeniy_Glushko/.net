﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
   public class DigitCalc:ICalculation
    {
        private delegate double OperationDelegate(double arg1, double arg2);
        private readonly Dictionary<Operation, OperationDelegate> _operations;

        public DigitCalc()
        {
            _operations = new Dictionary<Operation, OperationDelegate>
               {
            { Operation.Addition, Addition },
            { Operation.Subtract, Subtract },
            {Operation.Multiply, Multiply },
            { Operation.Devide, Devide },
               };
        }
        public string PerformOperation(string arg1, string arg2, Operation oper)
        {
            if (!_operations.ContainsKey(oper))
                throw new ArgumentException($"Operation {oper} is invalid", nameof(oper));
            return Convert.ToString(_operations[oper](Convert.ToDouble(arg1), Convert.ToDouble(arg2)), CultureInfo.InvariantCulture);
        }

       private double Subtract(double arg1, double arg2)
        {
            return arg1 - arg2;
        }

       private double Addition(double arg1, double arg2)
        {
            return arg1 + arg2;
        }

       private double Devide(double arg1, double arg2)
        {
            return arg1 / arg2;
        }

       private double Multiply(double arg1, double arg2)
        {
            return arg1 * arg2;
        }
    }
}
