﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public interface ICalculation
    {
        string PerformOperation(string arg1, string arg2, Operation oper);
        //string PerformOperation(string arg1, Operation oper);
    }


}
