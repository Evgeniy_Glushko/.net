﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Schema;
using CalculatorBl;
namespace CalculatorNew
{
    class Program
    {

        private static void Main(string[] args)
        {

            Calculator calculator = new Calculator(false);
            while (true)
            {
                Console.WriteLine(calculator.Current);
                ConsoleKeyInfo key = Console.ReadKey(true);
                calculator.InvokeBtn(key.KeyChar);
                Console.Clear();
                Console.WriteLine(calculator.Log);
            }
        }
    }
}
