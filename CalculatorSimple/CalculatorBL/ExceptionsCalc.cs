﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBL
{
   public class ExceptionsCalc:Exception
    {
        public ExceptionsCalc() : base() {            
        }
        public ExceptionsCalc (string message) : base(message) { }
    }
}
