﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class MyEventArgs:EventArgs
    {
        public string Message { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", Message);
        }
    }
}
