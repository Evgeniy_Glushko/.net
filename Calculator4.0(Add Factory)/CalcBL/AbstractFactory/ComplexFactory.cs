﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class ComplexFactory:AbstractFactory
    {
        public override ICalculation CreateCalculation()
        {
            return new ComplexCalc();
        }

        public override AbstractInvoker CreateInvoker()
        {
            return new ComplexInvoker();
        }
    }
}
