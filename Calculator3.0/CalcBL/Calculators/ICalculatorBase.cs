﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public interface ICalculatorBase
    {
        string PerformOperation(string arg1, string arg2, Operation oper);
        string Addition(string arg1, string arg2);
        string Devide(string arg1, string arg2);
        string Multiply(string arg1, string arg2);
        string Subtract(string arg1, string arg2);
    }


}
