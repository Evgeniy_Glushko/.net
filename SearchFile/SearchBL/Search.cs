﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchBL
{
    public class Search
    {
        Dictionary<FileInfo, long> _inputFiles = new Dictionary<FileInfo, long>();
        readonly Dictionary<long, IList<FileInfo>> _sameSizeFiles = new Dictionary<long, IList<FileInfo>>();
        const int BYTES = sizeof(Int64);
        const int BytesToRead = sizeof(Int64);
        readonly IList<IList<FileInfo>> _sameEqualFiles = new List<IList<FileInfo>>();

        public IList<IList<FileInfo>> SameEqualFiles
        {
            get { return this._sameEqualFiles; }
        }

        public bool FilesAreEqual(FileInfo first, FileInfo second)
        {
            if (first.Length != second.Length)
                return false;
            int iterations = (int)Math.Ceiling((double)first.Length / BytesToRead);
            FileStream fs1 = first.OpenRead();
            FileStream fs2 = second.OpenRead();
            byte[] one = new byte[BytesToRead];
            byte[] two = new byte[BytesToRead];

            for (int i = 0; i < iterations; i++)
            {
                fs1.Read(one, 0, BytesToRead);
                fs2.Read(two, 0, BytesToRead);
                if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                    return false;
            }
            fs1.Close();
            fs2.Close();
            return true;
        }
        public void SearchEqual(DirectoryInfo directory)
        {
            FileInfo[] files = directory.GetFiles("*", SearchOption.AllDirectories);
            foreach (var item in files)
            {
                _inputFiles.Add(item, item.Length);
            }

            _inputFiles = _inputFiles.OrderBy(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            CreateSizeArray();
        }

        private void CreateSizeArray()
        {
            IList<FileInfo> list = new List<FileInfo>();
            IList<FileInfo> list1 = new List<FileInfo>();
            int x = 0;
            int m = 0;
            int i;
            for (i = 0; i < _inputFiles.Count; i++)
            {
                if (_inputFiles.ElementAt(x).Value == _inputFiles.ElementAt(i).Value)
                {
                    list.Add(_inputFiles.ElementAt(i).Key);
                    if (i == _inputFiles.Count - 1)
                    {
                        if (!_sameSizeFiles.ContainsKey(_inputFiles.ElementAt(x).Value) && list.Count > 1)
                        {
                            list1 = list;
                            _sameSizeFiles.Add(_inputFiles.ElementAt(x).Value, list1);
                        }
                    }

                }
                else
                {
                    if (!_sameSizeFiles.ContainsKey(_inputFiles.ElementAt(x).Value) && list.Count > 1)
                    {
                        list1 = list;
                        _sameSizeFiles.Add(_inputFiles.ElementAt(x).Value, list1);
                    }
                    x = i;
                    i--;
                    list = new List<FileInfo>();
                }
                m++;

            }
            GroupsBySize(_sameSizeFiles);
        }
        public void GroupsBySize(Dictionary<long, IList<FileInfo>> arr)
        {
            foreach (var item in arr)
            {
                FilesCompares(item.Value);
            }
        }
        private void FilesCompares(IList<FileInfo> value)
        {
            IList<FileInfo> list = new List<FileInfo>();
            foreach (var item in value)
            {
                foreach (FileInfo itemInfo in value)
                {
                    if (item == itemInfo || (list.Contains(item) && list.Contains(itemInfo)))
                        continue;
                    var state = FilesAreEqual(item, itemInfo);
                    if (!state) continue;
                    if (!list.Contains(item))
                        list.Add(item);
                    if (!list.Contains(itemInfo))
                        list.Add(itemInfo);
                }
            }
            _sameEqualFiles.Add(list);
        }
    }
}
