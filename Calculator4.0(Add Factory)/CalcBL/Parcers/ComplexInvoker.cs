﻿using System.Text.RegularExpressions;

namespace CalcBL
{
    public class ComplexInvoker:AbstractInvoker
    {
        protected override bool IsDigit(char button)
        {
            if (char.IsDigit(button) || button.Equals('.') || button.Equals(' '))
                return true;
            return false;
        }

        protected override void BtnDigit(char btn)
        {
            int amountDot = new Regex(@"\.").Matches(Current).Count;
            int amountSpace = new Regex(@" ").Matches(Current).Count;
            ChekNewline(btn);
            if(!Clear)
            {
                if ((btn.Equals('.') && amountDot > 2) || (btn.Equals(' ') && amountSpace > 1))
                    return;
                Current += btn;
            }
            Clear = false;
        }

    }
}
