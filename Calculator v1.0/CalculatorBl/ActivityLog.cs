﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBl
{
    class ActivityLog
    {

        private string log;
        private int counter = 0;
        public string Log
        {
            get { return this.log; }
        }

        public void AddLog(string arg1, string arg2, string operation, string res)
        {
            if (counter > 9)
            {
                Clear();
                counter = 0;
            }
            this.log += arg1 + operation + arg2 + " = \n" + res + "\n\n";
            counter++;
        }

        public void AddLog(string arg1, string operation, string res)
        {
            if (counter > 9)
            {
                Clear();
                counter = 0;
            }
            this.log += operation + "(" + arg1 + ")" + " = \n" + res + "\n";
            counter++;
        }

        public void Clear()
        {
            log = null;
        }
    }
}
