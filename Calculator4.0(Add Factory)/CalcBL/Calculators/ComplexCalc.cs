﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CalcBL
{
    public class ComplexCalc: ICalculation
    {
        private delegate Complex OperationDelegate(Complex arg1, Complex arg2);
        private readonly Dictionary<Operation, OperationDelegate> _operations;

        public ComplexCalc()
        {
            _operations = new Dictionary<Operation, OperationDelegate>
               {
            { Operation.Addition, Addition },
            { Operation.Subtract, Subtract },
            {Operation.Multiply, Multiply },
            { Operation.Devide, Devide },
               };
        }
        public string PerformOperation(string arg1, string arg2, Operation oper)
        {
            if (!_operations.ContainsKey(oper))
                throw new ArgumentException($"Operation {oper} is invalid", nameof(oper));
            return Convert.ToString(_operations[oper](arg1.ConvertTo(), arg2.ConvertTo()));
        }

        private Complex Subtract(Complex arg1, Complex arg2)
        {
            return arg1 - arg2;
        }

        private Complex Addition(Complex arg1, Complex arg2)
        {
            return arg1 - arg2;
        }

        private Complex Devide(Complex arg1, Complex arg2)
        {
            return arg1 - arg2;
        }

        private Complex Multiply(Complex arg1, Complex arg2)
        {
            return arg1 - arg2;
        }
    }
}

