﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public struct Complex
    {
        private double real;
        private double imaginary;
        public double Real
        {
            get
            {
                return this.real;
            }
        }
        public double Imaginary
        {
            get
            {
                return this.imaginary;
            }
        }

        public Complex(double real, double imaginary)
        {
            this.real = real;
            this.imaginary = imaginary;
        }

        public static Complex operator -(Complex value)
        {
            return new Complex(-value.real, -value.imaginary);
        }
        public static Complex operator +(Complex left, Complex right)
        {
            return new Complex(left.real + right.real, left.imaginary + right.imaginary);
        }
        public static Complex operator -(Complex left, Complex right)
        {
            return new Complex(left.real - right.real, left.imaginary - right.imaginary);
        }
        public static Complex operator *(Complex left, Complex right)
        {
            return new Complex((left.real * right.real) - (left.imaginary * right.imaginary), (left.imaginary * right.real) + (left.real * right.imaginary));
        }
        public static Complex operator /(Complex left, Complex right)
        {
            double real = left.real;
            double imaginary = left.imaginary;
            double num3 = right.real;
            double num4 = right.imaginary;
            if (Math.Abs(num4) < Math.Abs(num3))
            {
                double num5 = num4 / num3;
                return new Complex((real + (imaginary * num5)) / (num3 + (num4 * num5)), (imaginary - (real * num5)) / (num3 + (num4 * num5)));
            }
            double num6 = num3 / num4;
            return new Complex((imaginary + (real * num6)) / (num4 + (num3 * num6)), (-real + (imaginary * num6)) / (num4 + (num3 * num6)));
        }

        public override string ToString()
        {
            string sign;
            if (imaginary >= 0)
                sign = "+";
            else
            {
                sign = "-";
            }
            if (Math.Abs(imaginary) == 1)
                return string.Format(CultureInfo.CurrentCulture, "{0} {1} j", real, sign);
            else
                return string.Format(CultureInfo.CurrentCulture, "{0} {2} j{1}", real, Math.Abs(imaginary), sign);
        }
    }
}
