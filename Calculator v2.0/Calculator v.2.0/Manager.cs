﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL;

namespace Calculator_v._2._0
{
    class Manager
    {
        private AbstractCalculator calc;
        private string res;
        public Manager(AbstractCalculator calc)
        {
            this.calc = calc;
            calc.ShowLog += ShowResults;
        }

        private void ShowResults(object sender, MyEventArgs e)
        {
            //Console.Clear();
            res = e.Message;
        }

        public void DoWork()
        {
            Parcer calculator = new Parcer(this.calc);
            while (true)
            {
                try
                {
                    Console.WriteLine(calculator.Current);
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    calculator.InvokeBtn(key.KeyChar);
                    Console.Clear();
                    Console.WriteLine(res);
                    // Console.WriteLine(calculator.Log);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                }
            }
        }
    }
}
