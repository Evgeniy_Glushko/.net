﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HandBookBL
{
    [Serializable]
    [XmlInclude(typeof(Book))]
    public class Collection : IEnumerable<Book>
    {
        private readonly List<Book> _listOfBooks = new List<Book>();
        public event EventHandler MyEvent;
        public void Add(Book item)
        {
            _listOfBooks.Add(item);
            OnMyEvent();
        }
        public void Add(object item)
        {
            Add(item as Book);
        }
        public Book Search(string value)
        {
            foreach (var item in _listOfBooks)
            {
                if (item.Title == value)
                    return item;
            }
            return null;
        }
        public bool RemoveByTitle(string value)
        {
            for (int i = 0; i < _listOfBooks.Count; i++)
            {
                if (_listOfBooks[i].Title == value)
                {
                    _listOfBooks.RemoveAt(i);
                    OnMyEvent();
                    return true;
                }
            }
            return false;
        }
        public void Sort()
        {
            _listOfBooks.Sort();
        }
        public void Sort(IComparer<Book> comparer)
        {
            _listOfBooks.Sort(comparer);
        }

        public bool ChangeYear(string title, int year)
        {
            Book book = Search(title);
            if (book == null)
                return false;
            else book.Year = year;
            return true;
        }

        public void Clear()
        {
            _listOfBooks.Clear();
        }
        public IEnumerator<Book> GetEnumerator()
        {
            return _listOfBooks.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected virtual void OnMyEvent()
        {
            var handler = MyEvent;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
