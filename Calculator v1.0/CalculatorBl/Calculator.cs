﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CalculatorBl
{
    //Перечисление 
    public class Calculator
    {
        protected Operation lastOperation = Operation.None;
        protected string currentValue = "0";
        protected string previousValue;
        protected string memoryValue;
        protected bool clear = true;
        public string btnContent = "";
        private bool complexMode = false;
        public string Log
        {
            get { return this.activityLog.Log; }
        }

        public Calculator(bool mode)
        {
            if (mode)
                this.complexMode = true;

        }

        protected Calculation calculation = new Calculation();
        private ActivityLog activityLog = new ActivityLog();

        public string Current
        {
            get { return this.currentValue; }
        }

        public void InvokeBtn(char button)
        {
            btnContent = Convert.ToString(button);
            int btnInt = (int)button;
            ButtonType btn = (ButtonType)btnInt;
            Processing(btn);
        }
        protected void Processing(ButtonType btn)
        {
            if (BtnIsOperation(btn))
                BtnOperation(btn);
            else if (BtnIsDigit(btn))
                BtnDigit(btn);
            else
                return;
        }

        public bool BtnIsOperation(ButtonType btn)
        {
            foreach (Operation val in Enum.GetValues(typeof(Operation)))
            {
                if (val == (Operation)btn)
                {
                    return true;
                }
            }
            return false;

        }

        protected bool BtnIsDigit(ButtonType btn)
        {
            foreach (ButtonType val in Enum.GetValues(typeof(ButtonType)))
            {
                if (val == btn)
                {
                    return true;
                }
            }
            return false;
        }

        public void BtnDigit(ButtonType btn)
        {

            int amount = new Regex(@"\.").Matches(currentValue).Count;
            //int amountSpace = new Regex(@" ").Matches(currentValue).Count;
            if (clear)
            {
                if (btn == ButtonType.Delimeter || (btn == ButtonType.Spacebar && complexMode == true))
                {
                    currentValue = "0" + btnContent;
                }
                else
                    currentValue = btnContent;
            }
            else
            {
                if ((btn == ButtonType.Delimeter && currentValue.IndexOf(".") >= 0 && complexMode == false) || (btn == ButtonType.Spacebar && currentValue.IndexOf(" ") >= 0))

                    return;
                if (btn == ButtonType.Delimeter && amount >= 2)
                {
                    return;
                }
                currentValue += btnContent;
            }
            clear = false;
        }

        public void BtnOperation(ButtonType btn)
        {
            Operation oper = (Operation)btn;
            switch (oper)
            {
                case Operation.Delete:
                    if (currentValue.Length > 1 && clear == false)
                    {
                        currentValue = currentValue.Remove(currentValue.Length - 1, 1);
                        clear = false;
                        return;
                    }
                    else
                    {
                        currentValue = "0";
                        clear = true;
                    }
                    break;
                default:
                    Calculate(oper);
                    clear = true;
                    break;
            }
        }

        protected void Calculate(Operation oper, bool exprOne = false)
        {
            if (exprOne)
                lastOperation = oper;
            if (!clear)
            {
                Calculate();
                previousValue = currentValue;
            }
            lastOperation = oper;
        }

        protected void Calculate()
        {
            if (lastOperation == Operation.None || lastOperation == Operation.Equal)
                return;
            currentValue = Convert.ToString(CalculateValue(lastOperation));
        }
        protected dynamic CalculateValue(Operation oper)
        {
            dynamic prev;
            dynamic curr;
            if (complexMode)
            {
                prev = ConvertToComplex(previousValue);
                curr = ConvertToComplex(currentValue);
            }
            else
            {
                prev = Convert.ToDouble(previousValue);
                curr = Convert.ToDouble(currentValue);
            }
            dynamic res = 0;

            switch (oper)
            {
                case Operation.Addition:
                    res = calculation.Addition(prev, curr);
                    activityLog.AddLog(previousValue, currentValue, " + ", Convert.ToString(res));
                    return res;
                case Operation.Devide:
                    res = calculation.Devide(prev, curr);
                    activityLog.AddLog(previousValue, currentValue, " / ", Convert.ToString(res));
                    return res;
                case Operation.Multiply:
                    res = calculation.Multiply(prev, curr);
                    activityLog.AddLog(previousValue, currentValue, " * ", Convert.ToString(res));
                    return res;
                case Operation.Subtract:
                    res = calculation.Subtract(prev, curr);
                    activityLog.AddLog(previousValue, currentValue, " - ", Convert.ToString(res));
                    return res;
                default: return res;
            }
        }
        public Complex ConvertToComplex(string str)
        {
            string real = "";
            string imaginary = "";
            string sign = "";
            var regex = new Regex(@"(?<num>^[0-9]{1,30}\.{0,1}[0-9]{0,20}) (?<num1>[0-9]{0,30}\.{0,1}[0-9]{0,20})");
            var regexNew = new Regex(@"(?<num>^[0-9]{1,30}\.{0,1}[0-9]{0,20})(?<sign> [+-]{1}) j{1}(?<num1>[0-9]{0,30}\.{0,1}[0-9]{0,20})");
            if (regexNew.IsMatch(str))
            {
                foreach (Match s in regexNew.Matches(str))
                {
                    real = Convert.ToString(s.Groups["num"]);
                    imaginary = Convert.ToString(s.Groups["num1"]);
                    sign = Convert.ToString(s.Groups["sign"]);
                }
                if (imaginary == "")
                {
                    imaginary = "1";
                }
                if ((int)sign.ToCharArray()[0] == 32)
                {
                    imaginary = "-" + imaginary;
                }
            }
            else
            {
                foreach (Match s in regex.Matches(str))
                {
                    real = Convert.ToString(s.Groups["num"]);
                    imaginary = Convert.ToString(s.Groups["num1"]);
                }
            }
            return new Complex(Convert.ToDouble(real), Convert.ToDouble(imaginary));

        }
    }
}
