﻿namespace CalcBL
{
    public class ParcerEventArgs
    {
        public string Arg1 { get; }
        public string Arg2 { get; }
        public Operation Oper { get; }
        public ParcerEventArgs(string arg1, string arg2, Operation oper)
        {
            Arg1 = arg1;
            Arg2 = arg2;
            Oper = oper;
        }


    }
}
