﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class DigitCalc : AbstractCalculator
    {
        protected override dynamic ConvertToDesiredType(string str)
        {
            return Convert.ToDouble(str);
        }
    }
}
