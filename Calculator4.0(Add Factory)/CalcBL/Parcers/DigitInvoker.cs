﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class DigitInvoker : AbstractInvoker
    {
        protected override bool IsDigit(char button)
        {
            return char.IsDigit(button) || button == '.';
        }

        protected override void BtnDigit(char btn)
        {
            ChekNewline(btn);
            if(!Clear)
            {
                if (btn == '.' && Current.IndexOf(".", StringComparison.Ordinal) >= 0)
                    return;
                Current += btn;
            }
            Clear = false;
        }

     
    }
}
