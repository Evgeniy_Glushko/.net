﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CalcBL
{
   public class ComplexCalc: AbstractCalculator
    {
       protected override dynamic ConvertToDesiredType(string str)
        {
            string real = "";
            string imaginary = "";
            string sign = "";
            var regex = new Regex(@"(?<num>^[0-9]{1,30}\.{0,1}[0-9]{0,20}) (?<num1>[0-9]{0,30}\.{0,1}[0-9]{0,20})");
            var regexNew = new Regex(@"(?<num>^[0-9]{1,30}\.{0,1}[0-9]{0,20})(?<sign> [+-]{1}) j{1}(?<num1>[0-9]{0,30}\.{0,1}[0-9]{0,20})");
            if (regexNew.IsMatch(str))
            {
                foreach (Match s in regexNew.Matches(str))
                {
                    real = Convert.ToString(s.Groups["num"]);
                    imaginary = Convert.ToString(s.Groups["num1"]);
                    sign = Convert.ToString(s.Groups["sign"]);
                }
                if (imaginary == "")
                {
                    imaginary = "1";
                }
                if ((int)sign.ToCharArray()[0] == 32)
                {
                    imaginary = "-" + imaginary;
                }
            }
            else
            {
                foreach (Match s in regex.Matches(str))
                {
                    real = Convert.ToString(s.Groups["num"]);
                    imaginary = Convert.ToString(s.Groups["num1"]);
                }
            }
            return new Complex(Convert.ToDouble(real), Convert.ToDouble(imaginary));
        }
    }
}
