﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    [Serializable]
    public delegate string MyEventHandler<TEventArgs>(object sender, TEventArgs e);
    public abstract class AbstractParcer
    {
        private Operation _lastOperation = Operation.None;
        private string _currentValue = "0";
        private string _previousValue;
        private bool _clear = true;
        private string _btnContent = "";
        public event MyEventHandler<ParcerEventArgs> CalculateEvent;

        public string BtnContent
        {
            get { return this._btnContent; }
            set { this._btnContent = value; }
        }

        public bool Clear
        {
            get { return this._clear; }
            set { this._clear = value; }
        }

        public string Current
        {
            get { return this._currentValue; }
            set { this._currentValue = value; }
        }

        public string PreviousValue
        {
            get { return this._previousValue; }
            set { this._previousValue = value; }
        }
        public Operation LastOperation
        {
            get { return this._lastOperation; }
            set { this._lastOperation = value; }
        }

        public void InvokeBtn(char button)
        {

            int btnInt = (int)button;
            Operation btn = (Operation)btnInt;
            _btnContent = Convert.ToString(button);
            if (IsDigit(button))
                BtnDigit(button);
            else if (BtnIsOperation(btn))
                BtnOperation(btn);
            else
                return;
        }

        protected abstract bool IsDigit(char button);

        protected abstract void BtnDigit(char button);

        protected abstract bool BtnIsOperation(Operation btn);

        public void BtnOperation(Operation btn)
        {
            switch (btn)
            {
                case Operation.Delete:
                    if (_currentValue.Length > 1 && _clear == false)
                    {
                        _currentValue = _currentValue.Remove(_currentValue.Length - 1, 1);
                        _clear = false;
                        return;
                    }
                    else
                    {
                        _currentValue = "0";
                        _clear = true;
                    }
                    break;
                default:
                    Calculate(btn);
                    _clear = true;
                    break;
            }
        }
        private void Calculate(Operation oper, bool exprOne = false)
        {
            if (exprOne)
                _lastOperation = oper;
            if (!_clear)
            {
                Calculate();
                _previousValue = _currentValue;
            }
            _lastOperation = oper;
        }

        private void Calculate()
        {
            if (_lastOperation == Operation.None || _lastOperation == Operation.Equal)
                return;
            _currentValue = CalculateValue(_lastOperation);
        }

        private string CalculateValue(Operation oper)
        {
            return OnCalculateEvent(new ParcerEventArgs(PreviousValue, Current, oper));
        }


        protected virtual string OnCalculateEvent(ParcerEventArgs e)
        {
            var handler = CalculateEvent;
            if (handler != null)
                return handler.Invoke(this, e);
            return null;
        }
    }
}
