﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBL
{
    static class ExtensionClass
    {
        public static Operation  ConvertToButtonType (this string oper)
        {
            switch (oper)
            {
                case "+":
                    return Operation.Addition;
                case "/":
                    return Operation.Devide;
                case "-":
                    return Operation.Subtract;
                case "*":
                    return Operation.Multiply;
                default: throw new ExceptionsCalc("Operation not found");
            }
        }
    }
}
