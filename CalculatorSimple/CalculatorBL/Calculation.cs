﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorBL
{
    interface ICalculationSimple
    {
        double Subtract (double arg1, double arg2);
        double Addition (double arg1, double arg2);
        double Multiply (double arg1, double arg2);
        double Devide (double arg1, double arg2);
    }

    public class Calculation : ICalculationSimple
    {
        public double Subtract (double arg1, double arg2)
        {
            return arg1 - arg2;
        }
        public double Addition (double arg1, double arg2)
        {
            return arg1 + arg2;
        }
        public double Multiply (double arg1, double arg2)
        {
            return arg1 * arg2;
        }
        public double Devide (double arg1, double arg2)
        {
            if (arg2 == 0)
            {
                throw new DivideByZeroException();
            }
            return arg1 / arg2;
        }
    }
}
