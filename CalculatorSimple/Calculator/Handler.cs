﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorBL;

namespace Calculator
{
    class Handler
    {
        CalcBl handler = new CalcBl();

        public void Invoke ()
        {
            try
            {
                Console.WriteLine("First argument");
                double arg1 = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Second argument");
                double arg2 = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Sign operation");
                string sign = Console.ReadLine();
                string res = handler.Calculate(arg1, arg2, sign);
                Console.WriteLine(res);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ExceptionsCalc e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();

        }

    }
}
