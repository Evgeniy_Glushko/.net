﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
   public class DigitFactory:AbstractFactory
    {
        public override ICalculation CreateCalculation()
        {
            return new DigitCalc();
        }

        public override AbstractInvoker CreateInvoker()
        {
            return new DigitInvoker();
        }
    }
}
