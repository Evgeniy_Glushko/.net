﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HandBookBL
{
    [Serializable]
    public class Book : IComparable, IComparable<Book>, IEquatable<Book>
    {
        private string _title ;
        private string _author;
        private int _year;

        public string Title {
            get { return this._title; }
            set { this._title = value; }
        }
        public string Author {
            get { return this._author; }
            set { this._author = value; }
        }
        public int Year {
            get { return this._year; }
            set
            {
                if(value < 1950 || value > 2015)
                    throw new ArgumentOutOfRangeException("value", value, "Wrong input");
                this._year = value;
            }
        }
        public Book(string title, string author, int year)
        {
            this.Title = title;
            this.Author = author;
            this.Year = year;
        }
        public Book()
        {
        }
        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            return Equals(obj as Book);
        }
        public override int GetHashCode()
        {
            return Title.GetHashCode() ^ Author.GetHashCode() ^ Year;
        }
        
        public int CompareTo(object obj)
        {
            return CompareTo(obj as Book);
        }

        public int CompareTo(Book other)
        {
            if (Equals(other, this))
                return 0;
            return String.Compare(_author.ToUpper(), other._author.ToUpper(), StringComparison.Ordinal);
        }

        public bool Equals(Book other)
        {
            return (string.Equals(_author, other._author) && string.Equals(_title,other._title) && _year == other._year);
        }

        public override string ToString()
        {
            return string.Format("{0,10}\t{1}\t{2,10}", this.Title, this.Author, this.Year);
        }

        //public static bool operator ==(Book book1, Book book2)
        //{
        //    return book1.Equals(book2);
        //}

        //public static bool operator !=(Book book1, Book book2)
        //{
        //    return !(book1 == book2);
        //}
    }
}
