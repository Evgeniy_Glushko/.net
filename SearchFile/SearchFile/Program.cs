﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SearchBL;
using System.Diagnostics;

namespace SearchFile
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo file = new DirectoryInfo("test1");
            Search search = new Search();
            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            search.SearchEqual(file);
            sWatch.Stop();
            //Console.WriteLine(sWatch.ElapsedMilliseconds.ToString());
            FileHandler fileHandler = new FileHandler();
            fileHandler.SaveToFile("new.txt",search.SameEqualFiles);
            fileHandler.FileCompression("new.txt");
            IList<IList<FileInfo>> sameEqualFiles = search.SameEqualFiles;
            foreach (var variable in search.SameEqualFiles)
            {
                foreach (var variable1 in variable)
                {
                    Console.WriteLine(variable1.FullName);
                    
                }
                Console.WriteLine("-----");
            }
            Console.ReadKey();
        }
    }
}
